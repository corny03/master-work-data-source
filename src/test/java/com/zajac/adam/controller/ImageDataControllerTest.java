package com.zajac.adam.controller;

import com.zajac.adam.entity.ImageEntity;
import com.zajac.adam.service.EntityService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import javax.persistence.EntityNotFoundException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

/**
 * Main controller test
 * Created by zajac on 21.02.2018.
 */
public class ImageDataControllerTest {

    private final byte[] sampleBytes = {12, 32, 43, 54, 65};
    @Mock
    private EntityService entityService;
    @Mock
    private MockMvc mockMvc;
    private ImageDataController imageDataController;

    @Before
    public void setUp() throws Exception {
        entityService = Mockito.mock(EntityService.class);
        imageDataController = new ImageDataController(entityService);
        mockMvc = MockMvcBuilders
                .standaloneSetup(imageDataController)
                .setControllerAdvice(new GlobalDefaultExceptionHandler())
                .build();
    }

    @Test
    public void getImage() throws Exception {
        //given
        final String jpgFileName = "some.jpg";
        final String pngFileName = "some.png";
        String existingId = "id1";
        String existingIdTwo = "id2";
        String existingIdThree = "id3";
        String notExistingId = "Not Existing ID";
        String imageGetEndpoint = "/";
        final Timestamp timestamp = new Timestamp(1517949900000L);
        ImageEntity imageJpgEntity = new ImageEntity(existingId,
                jpgFileName,
                "",
                "testUSer",
                false,
                timestamp,
                sampleBytes);
        ImageEntity imagePngEntity = new ImageEntity(existingId,
                pngFileName,
                "",
                "testUSer",
                false,
                timestamp,
                sampleBytes);
        ImageEntity imageNotSpecifiedEntity = new ImageEntity(existingId,
                "someImageWithNotSpecifiedFormat",
                "",
                "testUSer",
                false,
                timestamp,
                sampleBytes);
//when
        Mockito.when(entityService.getImageEntityFromDatabase(existingId)).thenReturn(imageJpgEntity);
        Mockito.when(entityService.getImageEntityFromDatabase(existingIdTwo)).thenReturn(imagePngEntity);
        Mockito.when(entityService.getImageEntityFromDatabase(existingIdThree)).thenReturn(imageNotSpecifiedEntity);
        Mockito.doThrow(EntityNotFoundException.class).when(entityService).getImageEntityFromDatabase(notExistingId);
//then

        mockMvc.perform(get(imageGetEndpoint + existingId))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.IMAGE_JPEG))
                .andExpect(MockMvcResultMatchers.content().bytes(sampleBytes))
                .andExpect(MockMvcResultMatchers.header().string("Content-Length", "5"))
                .andExpect(MockMvcResultMatchers.header()
                        .stringValues("Content-Disposition",
                                "form-data; name=\"attachment\"; filename=\"" + jpgFileName + "\""));

        mockMvc.perform(get(imageGetEndpoint + existingIdTwo))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.IMAGE_PNG))
                .andExpect(MockMvcResultMatchers.content().bytes(sampleBytes))
                .andExpect(MockMvcResultMatchers.header().string("Content-Length", "5"))
                .andExpect(MockMvcResultMatchers.header()
                        .stringValues("Content-Disposition",
                                "form-data; name=\"attachment\"; filename=\"" + pngFileName + "\""));

        mockMvc.perform(get(imageGetEndpoint + notExistingId))
                .andExpect(MockMvcResultMatchers.status().isNotFound());

        mockMvc.perform(get(imageGetEndpoint + existingIdThree))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void singleUpload() throws Exception {
        //given
        String userName = "testUser";
        String uploadEndpoint = "/upload";
        String goodFileName = "some.jpg";
        String wrongFileName = "badFormat.pdf";
        byte[] nullBytes = {};
        MultiValueMap<String, String> goodParams = new LinkedMultiValueMap<>();
        goodParams.add("userName", userName);
        goodParams.add("isHidden", "false");
        MultiValueMap<String, String> badNameParams = new LinkedMultiValueMap<>(goodParams);
        badNameParams.add("filename", wrongFileName);
        goodParams.add("filename", goodFileName);
        final String idToReturn = "goodID";
        //when
        Mockito.when(
                entityService.uploadToDatabaseAndGetId(goodFileName, sampleBytes, userName, false))
                .thenReturn(idToReturn);
        //then
        mockMvc.perform(post(uploadEndpoint).content(sampleBytes).params(goodParams))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.content().string(idToReturn));

        mockMvc.perform(post(uploadEndpoint).content(sampleBytes).params(badNameParams))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());

        mockMvc.perform(post(uploadEndpoint).content(nullBytes).params(goodParams))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void getLastImages() throws Exception {
        //given
        final String lastImagesEndpoint = "/lasts12";
        List<String> ids = new ArrayList<>();
        ids.add("id1");
        ids.add("id2");
        //then
        mockMvc.perform(get(lastImagesEndpoint)).andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content()
                        .string("[]"));
        //when
        Mockito.when(entityService.getListOf12LastIdsUploadedImages()).thenReturn(ids);
        //then
        mockMvc.perform(get(lastImagesEndpoint)).andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content()
                        .string("[" + "\"" + ids.get(0) + "\"" + "," + "\"" + ids.get(1) + "\"" + "]"));
    }

    @Test
    public void getUserImages() throws Exception {
        //given
        final String userImagesEndpoint = "/by-user/";
        final String userNameOne = "UserNameOne";
        final String userNameTwo = "UserNameTwo";
        List<String> ids = new ArrayList<>();
        ids.add("id1");
        ids.add("id2");
        //when
        Mockito.when(entityService.getListOfIdsImagesByUser(userNameOne)).thenReturn(ids);
        //then
        mockMvc.perform(get(userImagesEndpoint + userNameOne)).andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content()
                        .string("[" + "\"" + ids.get(0) + "\"" + "," + "\"" + ids.get(1) + "\"" + "]"));
        mockMvc.perform(get(userImagesEndpoint + userNameTwo)).andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content()
                        .string("[]"));
    }

    @Test
    public void deleteById() throws Exception {
        //given
        final String deletedID = "deletedID";
        final String existingID = "existingID";
        final String deleteEndpoint = "/";
        //when
        Mockito.doThrow(EmptyResultDataAccessException.class).when(entityService).deleteFromDatabaseById(deletedID);
        //then
        mockMvc.perform(delete(deleteEndpoint + existingID)).
                andExpect(MockMvcResultMatchers.status().isOk());
        mockMvc.perform(delete(deleteEndpoint + deletedID)).
                andExpect(MockMvcResultMatchers.status().isGone());
    }
}
