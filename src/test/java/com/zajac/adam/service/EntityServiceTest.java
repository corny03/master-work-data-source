package com.zajac.adam.service;

import com.zajac.adam.dao.ImageDatabaseDAO;
import com.zajac.adam.entity.ImageEntity;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import javax.persistence.EntityNotFoundException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Timestamp;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;

/**
 * Created by zajac on 24.11.2017.
 * Controller main service test
 */
public class EntityServiceTest {

    private final String fileName = "lena.jpg";
    private final String fullImageEntityId = "fullImageEntityId";
    private ImageEntity fullImageEntity;
    private byte[] bytes;
    private EntityService entityService;
    @Mock
    private ImageDatabaseDAO imageDatabaseDAO;

    @Before
    public void setUp() throws Exception {
        imageDatabaseDAO = Mockito.mock(ImageDatabaseDAO.class);
        entityService = new EntityService(imageDatabaseDAO);
        Timestamp timestamp = new Timestamp(1517949900000L);
        this.bytes = Files.readAllBytes(Paths.get(fileName));
        fullImageEntity =
                new ImageEntity(fullImageEntityId,
                        fileName,
                        "descritption",
                        "someUser",
                        false,
                        timestamp,
                        bytes);
    }

    @Test
    public void getImageEntityFromDatabase() throws Exception {
        //given
        final String idTwo = "idTwo";
        Mockito.when(imageDatabaseDAO.findByUuid(fullImageEntityId)).thenReturn(fullImageEntity);
        //when
        ImageEntity returnedImageEntity = entityService.getImageEntityFromDatabase(fullImageEntityId);
        try {
            entityService.getImageEntityFromDatabase(idTwo);
            //then
            Assert.fail("Method didnt thrown EntityNotFoundException");
        } catch (EntityNotFoundException e) {
        }
        //then
        Assert.assertEquals(returnedImageEntity, fullImageEntity);
    }

    @Test
    public void uploadToDatabaseAndGetId() throws Exception {
        //given
        byte[] sampleBytes = {2, 2, 2};
        //when
        String notEmpty = entityService.uploadToDatabaseAndGetId("some.jpg", sampleBytes, "testUser", false);
        //then
        verify(imageDatabaseDAO, Mockito.times(1)).save(any(ImageEntity.class));
        Assert.assertFalse(notEmpty.isEmpty());
    }

    @Test
    public void deleteFromDatabaseById() throws Exception {
        //given
        final String someId = "someId";
        //when
        entityService.deleteFromDatabaseById(someId);
        // then
        verify(imageDatabaseDAO, Mockito.times(1)).delete(someId);
    }

    @Test
    public void getListOf12LastIdsUploadedImages() throws Exception {
        //when
        entityService.getListOf12LastIdsUploadedImages();
        // then
        verify(imageDatabaseDAO, Mockito.times(1)).findFirst12ByOrderByTimePointDesc();
    }

    @Test
    public void getListOfIdsImagesByUser() throws Exception {
        //given
        final String userName = "testUser";
        //when
        entityService.getListOfIdsImagesByUser(userName);
        // then
        verify(imageDatabaseDAO, Mockito.times(1)).findIdByUserName(userName);
    }
}