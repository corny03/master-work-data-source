package com.zajac.adam;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;


@EnableEurekaClient
@SpringBootApplication
public class DataSourceApplication {
	public static void main(String[] args) {
		SpringApplication.run(DataSourceApplication.class, args);
	}
}
