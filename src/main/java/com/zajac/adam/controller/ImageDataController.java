package com.zajac.adam.controller;

import com.zajac.adam.entity.ImageEntity;
import com.zajac.adam.exception.WrongDataException;
import com.zajac.adam.routing.Routing;
import com.zajac.adam.service.EntityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.List;

/**
 * Main service controller
 * Created by zajac on 05.08.2017.
 */
@RestController
public class ImageDataController {

    private final EntityService entityService;

    @Autowired
    public ImageDataController(EntityService entityService) {
        this.entityService = entityService;
    }

    /**
     * Method/Endpoint used to download image
     *
     * @param id id of image to return
     * @return status 200 when exist with bytes in body and basic info in header
     * status 404 when resource with id is not found
     */
    @GetMapping(Routing.IMAGE_URL)
    public ResponseEntity getImage(@PathVariable("id") String id) {
        ImageEntity imageEntity = entityService.getImageEntityFromDatabase(id);
        ByteArrayResource byteArrayResource = new ByteArrayResource(imageEntity.getBytes());
        HttpHeaders responseHeaders = prepareHeader(imageEntity.getName(), imageEntity.getBytes().length);
        return new ResponseEntity<>(byteArrayResource, responseHeaders, HttpStatus.OK);
    }

    /**
     * Method/Endpoint used to upload image
     *
     * @param fileName name of uploaded file
     * @param bytes    Image bytes
     * @param userName name of user logged in sending moment
     * @param isHidden option to hide image from main web page
     * @return status code 201 with created id in body when added image
     * status code 400 when some data is not valid
     * @throws WrongDataException but cached by controllerAdvice
     */
    @PostMapping(Routing.UPLOAD_URL)
    public ResponseEntity<String> singleUpload(@RequestParam("filename") String fileName,
                                               @RequestBody byte[] bytes,
                                               @RequestParam("userName") String userName,
                                               @RequestParam("isHidden") boolean isHidden) {
        if (!isBytesOk(bytes) || !isFileNameOk(fileName))
            throw new WrongDataException("data or file name wrong");
        String id = entityService.uploadToDatabaseAndGetId(fileName, bytes, userName, isHidden);
        String path = preparePath(id);

        return ResponseEntity.created(URI.create(path)).body(id);
    }

    /**
     * @return Ids last 12 images put with false isHidden flag
     */
    @GetMapping(Routing.LAST_IMAGES_IDS)
    @ResponseBody
    public List<String> getLastImages() {
        return entityService.getListOf12LastIdsUploadedImages();
    }

    /**
     * Return user images ids
     *
     * @param userName name of user what images urls want to get
     * @return Ids of user images
     */
    @GetMapping(Routing.USER_IMAGES_IDS)
    @ResponseBody
    public List<String> getUserImages(@PathVariable("userName") String userName) {
        return entityService.getListOfIdsImagesByUser(userName);
    }

    /**
     * @param id id of image to delete
     * @return http status 200 when deleted or 410(GONE) when resource deleted
     */
    @DeleteMapping(Routing.DELETE_URL)
    public ResponseEntity deleteById(@PathVariable String id) {
        entityService.deleteFromDatabaseById(id);
        return ResponseEntity.ok().build();
    }

    //  ### ### ### ### ### ### ### ### ###

    private HttpHeaders prepareHeader(String fileName, int dataLength) {
        HttpHeaders respHeaders = new HttpHeaders();
        setContextType(respHeaders, fileName);
        respHeaders.setContentLength(dataLength);
        respHeaders.setContentDispositionFormData("attachment", fileName);
        return respHeaders;
    }

    private void setContextType(HttpHeaders respHeaders, String fileName) {
        if (fileName.toLowerCase().contains("jpeg") || fileName.toLowerCase().contains("jpg")) {
            respHeaders.setContentType(MediaType.IMAGE_JPEG);
        } else if (fileName.toLowerCase().contains("png")) {
            respHeaders.setContentType(MediaType.IMAGE_PNG);
        } else
            throw new WrongDataException("Image found, but cant specify format");
    }

    private boolean isBytesOk(byte[] bytes) {
        return bytes != null && bytes.length > 1;
    }

    private boolean isFileNameOk(String fileName) {
        return (fileName.toLowerCase().contains(".jpg")
                || fileName.toLowerCase().contains(".jpeg")
                || fileName.toLowerCase().contains(".png"));
    }

    private String preparePath(String id) {
        String path = Routing.IMAGE_URL.split("[{]")[0];
        path += id;
        return path;
    }
}
