package com.zajac.adam.controller;

import com.zajac.adam.exception.WrongDataException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.persistence.EntityNotFoundException;

/**
 * Created by zajac on 29.11.2017.
 * global place for handling exceptions
 */
@ControllerAdvice
public class GlobalDefaultExceptionHandler {

    @ExceptionHandler(value = EmptyResultDataAccessException.class)
    public ResponseEntity entityIsDeleted(EmptyResultDataAccessException e) {
        return ResponseEntity.status(HttpStatus.GONE).body("image was deleted before");
    }

    @ExceptionHandler(EntityNotFoundException.class)
    public ResponseEntity notFoundEntityException(EntityNotFoundException e) {
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
    }

    @ExceptionHandler(WrongDataException.class)
    public ResponseEntity handleException(Exception e) {
        return ResponseEntity.badRequest().body(e.getMessage());
    }
}
