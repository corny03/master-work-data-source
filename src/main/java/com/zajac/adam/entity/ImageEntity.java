package com.zajac.adam.entity;

import org.hibernate.annotations.Type;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.sql.Timestamp;

/**
 * Entity object
 * Created by zajac on 05.08.2017.
 */
@Entity
public class ImageEntity {
    @Id
    private String uuid;
    private String name;
    private String description;
    private String userName;
    private boolean isHidden;
    @Column(name = "sent_timestamp")
    private Timestamp timestamp;
    @Type(type = "org.hibernate.type.MaterializedBlobType")
    private byte[] bytes;

    public ImageEntity() {
    }

    public ImageEntity(String uuid,
                       String name,
                       String description,
                       String userName,
                       boolean isHidden,
                       Timestamp timestamp,
                       byte[] bytes) {
        this.uuid = uuid;
        this.name = name;
        this.description = description;
        this.userName = userName;
        this.isHidden = isHidden;
        this.timestamp = timestamp;
        this.bytes = bytes;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public boolean isHidden() {
        return isHidden;
    }

    public void setHidden(boolean hidden) {
        isHidden = hidden;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }

    public byte[] getBytes() {
        return bytes;
    }

    public void setBytes(byte[] bytes) {
        this.bytes = bytes;
    }
}
