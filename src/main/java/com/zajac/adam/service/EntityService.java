package com.zajac.adam.service;

import com.zajac.adam.dao.ImageDatabaseDAO;
import com.zajac.adam.entity.ImageEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.List;
import java.util.UUID;

/**
 * Service operate on entities and work with database
 * Created by zajac on 22.11.2017.
 */
@Service
public class EntityService {
    private final ImageDatabaseDAO imageDatabaseDAO;

    @Autowired
    public EntityService(ImageDatabaseDAO imageDatabaseDAO) {
        this.imageDatabaseDAO = imageDatabaseDAO;
    }

    public ImageEntity getImageEntityFromDatabase(String id) {
        ImageEntity imageEntity = imageDatabaseDAO.findByUuid(id);

        if (imageEntity == null) {
            throw new EntityNotFoundException("not found entity with id " + id);
        }
        return imageEntity;
    }

    public String uploadToDatabaseAndGetId(String fileName, byte[] bytes, String userName, boolean isHidden) {
        String descriptionImageEntity = "Original image";
        Timestamp timestamp = new Timestamp(Calendar.getInstance().getTime().getTime());
        String uuid = UUID.randomUUID().toString();
        ImageEntity imageEntity =
                new ImageEntity(uuid, fileName, descriptionImageEntity, userName, isHidden, timestamp, bytes);
        imageDatabaseDAO.save(imageEntity);
        return uuid;
    }

    /**
     * Method throw EmptyResultDataAccessException when entity is not in database
     * Exception should be catch by controller advice
     *
     * @param id id of deleted entity
     */
    public void deleteFromDatabaseById(String id) {
        imageDatabaseDAO.delete(id);
    }

    public List<String> getListOf12LastIdsUploadedImages() {
        return imageDatabaseDAO.findFirst12ByOrderByTimePointDesc();
    }

    public List<String> getListOfIdsImagesByUser(String userName) {
        return imageDatabaseDAO.findIdByUserName(userName);
    }
}
