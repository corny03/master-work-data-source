package com.zajac.adam.dao;

import com.zajac.adam.entity.ImageEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Data Object Interface
 * used to automatic database connection between database and service
 * Created by zajac on 05.08.2017.
 */
public interface ImageDatabaseDAO extends CrudRepository<ImageEntity, String> {
    @Transactional
    ImageEntity findByUuid(String uuid);

    @Query(nativeQuery = true, value = "select uuid from image_entity " +
            "WHERE is_hidden = false " +
            "ORDER BY sent_timestamp DESC LIMIT 12")
    List<String> findFirst12ByOrderByTimePointDesc();

    @Query(nativeQuery = true, value = "select uuid from image_entity " +
            "WHERE user_name = :user_name " +
            "ORDER BY sent_timestamp DESC")
    List<String> findIdByUserName(@Param("user_name") String userName);
}
