package com.zajac.adam.routing;

/**
 * Place with defined service endpoints
 * Created by zajac on 10.08.2017.
 */
public class Routing {
    public static final String IMAGE_URL = "/{id}";
    public static final String UPLOAD_URL = "/upload";
    public static final String DELETE_URL = "/{id}";
    public static final String LAST_IMAGES_IDS = "/lasts12";
    public static final String USER_IMAGES_IDS = "/by-user/{userName}";
}
